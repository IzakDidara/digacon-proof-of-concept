const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

// Locations
const appSrc = ['./src/scripts/index.js', './src/styles/style.scss']; // Paths for the main entry points
const vendorSrc = ['react', './src/styles/vendor.scss']; // From node_modules or specific path

module.exports = {
    entry: {
        app: appSrc, // Entry point for main js and main css/scss
        vendor: vendorSrc, // Use this if you want to split vendors from other js. (create const with array of libraries used)
    },
    output: {
        path: path.resolve(__dirname, './../dist'), // Where to output the files
        filename: 'scripts/[name].js', // File name. [name] sets the name provided in entry, in this case it will be app.js, react.js, vendor.js and so on
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: [
            'node_modules',
            path.resolve(__dirname, './../src/scripts'),
            path.resolve('./../src'),
            'src/scripts'
        ]
    },
    module: {
        rules: [{
            enforce: "pre",
            test: /\.(js|jsx)$/, // regex for finding the right files
            exclude: [ // do not watch those 2 locations, you can also specify a include next to exclude
                path.resolve(__dirname, 'src/scripts/vendor'),
                /node_modules/
            ],
            use: [{
                loader: 'eslint-loader',
                options: {
                    configFile: path.join(__dirname, ".eslintrc"),
                }
            }]
        }, {
            test: /\.(js|jsx)$/, // regex for finding the right files
            exclude: [ // do not watch those 2 locations, you can also specify a include next to exclude
                path.resolve(__dirname, 'src/scripts/vendor'),
                /node_modules/
            ],
            use: [{
                loader: 'babel-loader',
                options: {
                    extends: path.join(__dirname, ".babelrc")
                }
            }]
        }, {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: 'css-loader!resolve-url-loader',
                publicPath: '/',
            })
        }, {
            test: /\.(sass|scss)$/,
            use: ExtractTextPlugin.extract({
                fallback: 'css-loader',
                use: [{
                    loader: 'css-loader',
                    options: {
                        url: 'false'
                    }
                }, {
                    loader: 'resolve-url-loader',
                    options: {
                        sourceMap: true
                    }
                }, {
                    loader: 'postcss-loader',
                    options: {
                        sourceMap: true,
                        options: { config: 'config/postcss.config.js' }
                    }
                }, {
                    loader: 'sass-loader',
                    options: {
                        sourceMap: true,
                        importLoaders: 1,
                        includePaths: ['src/']
                    }
                }],
                publicPath: '/',
            })
        }, {
            test: /\.html$/,
            use: ['html-loader']
        }, {
            test: /\.(jpg|png)$/,
            use: [
                {
                    loader: "file-loader",
                    options: {
                        name: '[name].[ext]', // To keep the same name use [name] and for same extension use [ext]
                        outputPath: 'images/',
                        publicPath: '/',
                    }
                }
            ]
        }, {
            test: /\.(woff2?|ttf|eot|svg)$/,
            use: [
                {
                    loader: "file-loader",
                    options: {
                        name: '[name].[ext]', // To keep the same name use [name] and for same extension use [ext]
                        outputPath: 'fonts/',
                        publicPath: '/',
                    }
                }
            ]
        }]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: `styles/[name].css`,
            allChunks: true,
        }),
        new webpack.optimize.CommonsChunkPlugin({ // Split your code into vendor
            name: 'vendor',
            minChunks: Infinity,
        }),
        new HtmlWebpackPlugin({
            inject: true,
            template: 'src/html/index.html', // What file does it take
            filename: 'html/index.html', // Where and with what name does it export
            chunks: ['app', 'vendor'] // If left empty injects nothing, remove chunks to inject everything
        })
        // new CleanWebpackPlugin(['dist/']), // Used to clean those paths
    ],

    devServer: {
        contentBase: path.join(__dirname, '../src/'),
        compress: true,
        port: 1991,
        historyApiFallback: {
            rewrites: [
                { from: /^\/$/, to: '/html/index.html' },
            ]
        },
        watchContentBase: true,
    }
};
