import api from 'core/api';

export function setCarsArray(items) {
    return {
        type: 'SET_CARS_ARRAY',
        items
    };
}

export function fetchAllCars() {
    return (dispatch) => {
        setTimeout(() => dispatch(setCarsArray(api.getAllCars())), 300);
    };
}
