function reducer(state = [], action) {
    switch (action.type) {
        case 'SET_CARS_ARRAY':
            return Object.assign({}, state, { items: action.items });
        default:
            return state;
    }
}

export default reducer;
