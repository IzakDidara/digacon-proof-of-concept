import { combineReducers } from 'redux';

import carReducer from './cars/reducer';

export default combineReducers({
    cars: carReducer
});
