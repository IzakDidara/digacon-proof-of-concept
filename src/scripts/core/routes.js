import React from 'react';
import { Route, Switch, Redirect } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';
import { history } from 'core/store';

// Routes
import CarsScreenContainer from 'app/screens/cars-screen/CarsScreenContainer';
import Header from 'app/shared/header/Header';
import Teaser from 'app/shared/teaser/Teaser';

const Routes = () => (
    <ConnectedRouter history={history}>
        <div>
            <Header />
            <Teaser />
            <main className="main">
                <div className="container">
                    <Switch>
                        <Route path="/cars" component={CarsScreenContainer} />
                        <Redirect to="/cars" />
                    </Switch>
                </div>
            </main>
        </div>
    </ConnectedRouter>
);

export default Routes;

