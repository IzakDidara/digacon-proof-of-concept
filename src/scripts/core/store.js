import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createHashHistory } from 'history';

import rootReducer from './reducers/reducers';

export const history = createHashHistory();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middlewares = applyMiddleware(thunk, routerMiddleware(history));

const defaultState = {
    cars: {
        items: []
    }
};

export default createStore(
    connectRouter(history)(rootReducer),
    defaultState,
    composeEnhancers(middlewares)
);
