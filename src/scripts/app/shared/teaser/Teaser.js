import React from 'react';
import './teaser.scss';

const Teaser = () => (
    <section className="teaser">
        <div className="teaser__image" />
    </section>
);

export default Teaser;
