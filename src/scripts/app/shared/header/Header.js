import React from 'react';
import { Link } from 'react-router-dom';
import './header.scss';

const Header = () => (
    <header className="header">
        <div className="container">
            <div className="header__inner">
                <h1 className="header__logo">
                    <Link to="/cars">Digacon</Link>
                </h1>
            </div>
        </div>
    </header>
);

export default Header;
