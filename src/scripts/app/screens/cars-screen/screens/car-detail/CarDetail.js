import React from 'react';
import PropTypes from 'prop-types';
import './car-detail.scss';

const CarDetail = ({ item }) => {
    if (!item) {
        return (<div>Nothing found</div>);
    }
    const { image, brand, model, currency, price, description } = item;
    return (
        <div className="car-detail">
            <div className="row">
                <div className="col-md-6">
                    <div className="car-detail__img-wrap">
                        <img className="car-detail__img" src={image} alt={brand} />
                    </div>
                </div>
                <div className="col-md-6">
                    <h3 className="car-detail__heading">{brand} {model}</h3>
                    <div className="car-detail__price">{currency} {price}</div>
                    <p>{description}</p>
                </div>
            </div>
        </div>
    );
};

CarDetail.propTypes = {
    item: PropTypes.shape({
        image: PropTypes.string,
        brand: PropTypes.string,
        model: PropTypes.string,
        currency: PropTypes.string,
        price: PropTypes.number,
        description: PropTypes.string
    })
};

export default CarDetail;
