import { createSelector } from 'reselect';

const carItemsSelector = state => state.cars.items;
const carIdSelector = (state, props) => props.match.params.carId;

export const activeCarSelector = createSelector(
    [carItemsSelector, carIdSelector],
    (carItems, carId) => carItems.find(item => parseFloat(item.id) === parseFloat(carId))
);
