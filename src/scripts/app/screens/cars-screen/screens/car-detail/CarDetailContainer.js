import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import CarDetail from './CarDetail';
import { activeCarSelector } from './selectors/selectors';

const actionCreators = {};

function mapStateToProps(state, props) {
    return {
        item: activeCarSelector(state, props)
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(CarDetail);
