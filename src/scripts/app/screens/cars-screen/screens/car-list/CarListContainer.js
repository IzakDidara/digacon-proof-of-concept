import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import CarList from './CarList';

const actionCreators = {};

function mapStateToProps(state) {
    return {
        carItems: state.cars.items
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(CarList);
