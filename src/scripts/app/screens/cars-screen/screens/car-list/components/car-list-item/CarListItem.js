import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './car-list-item.scss';

const CarListItem = ({ id, brand, model, price, currency, image, description }) => (
    <div className="car-list-item">
        <Link className="car-list-item__img-wrap" to={`/cars/${id}`}>
            <img className="car-list-item__img" src={image} alt={brand} />
        </Link>
        <div className="car-list-item__content">
            <Link className="car-list-item__title" to={`/cars/${id}`}>{brand} {model}</Link>
            <div className="car-list-item__price">{currency} {price}</div>
            <p>{description.slice(0, 100)}{description.length > 100 ? '...' : null}</p>
        </div>
    </div>
);

CarListItem.propTypes = {
    id: PropTypes.number,
    image: PropTypes.string,
    brand: PropTypes.string,
    model: PropTypes.string,
    currency: PropTypes.string,
    price: PropTypes.number,
    description: PropTypes.string
};

export default CarListItem;
