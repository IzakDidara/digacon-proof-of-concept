import React from 'react';
import PropTypes from 'prop-types';
import CarListItem from './components/car-list-item/CarListItem';

const CarList = ({ carItems }) => (
    <div className="row">
        {carItems && carItems.map(item => (
            <div key={item.id} className="col-lg-3 col-md-3 col-sm-4">
                <CarListItem {...item} />
            </div>
        ))}
    </div>
);

CarList.propTypes = {
    carItems: PropTypes.arrayOf(PropTypes.shape({
        image: PropTypes.string,
        brand: PropTypes.string,
        model: PropTypes.string,
        currency: PropTypes.string,
        price: PropTypes.number,
        description: PropTypes.string
    }))
};

export default CarList;
