import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchAllCars } from 'core/reducers/cars/actions';
import CarsScreen from './CarsScreen';

const actionCreators = { fetchAllCars };

function mapStateToProps() {
    return {};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(CarsScreen);
