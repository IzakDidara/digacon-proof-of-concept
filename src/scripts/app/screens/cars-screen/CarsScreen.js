import React, { Component } from 'react';
import { Route, Switch } from 'react-router';
import PropTypes from 'prop-types';

import CarDetailContainer from './screens/car-detail/CarDetailContainer';
import CarListContainer from './screens/car-list/CarListContainer';

class CarsScreen extends Component {
    componentDidMount() {
        const { fetchAllCars } = this.props;
        fetchAllCars();
    }

    render() {
        return (
            <Switch>
                <Route path="/cars" exact component={CarListContainer} />
                <Route path="/cars/:carId" component={CarDetailContainer} />
            </Switch>
        );
    }
}

CarsScreen.propTypes = {
    fetchAllCars: PropTypes.func
};

export default CarsScreen;
