#Readme

Start project by running '**npm install**' for the first time

Run '**npm run dev**' for dev server

Run '**npm run prod**' to generate files ready for deployment

p.s. webpack and webpack-dev-server need to be
installed globally ('npm install webpack webpack-dev-server -g')

